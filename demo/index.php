<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>

<head>
   <meta name='Content-Type' content='text/html;charset=UTF-8' />

   <title>MultipleSelect Demo</title>
  
   <link rel='stylesheet' type='text/css' href='multiselect_demo.css' />

   <script type='text/javascript' src='../libs/mootools.js'></script>
   <script type='text/javascript' src='../libs/mootoolsmore.js'></script>
   <script type='text/javascript' src='multiselect.js'></script>
   <script type='text/javascript'>
   
    document.addEvent('domready', function() {
        new MultipleSelect('myList');
        $('toggleLink').addEvent('click', function() {
            $('myList').show();
        });

    });

   </script>

</head>

<body>

    <h1>MultiSelect Demo</h1>

    In supporting browsers you will see the select-field replaced by a much fancier
    version. In non-supporting browsers the old select-field will be showed to ensure
    that your website remains useful. You can also show the old select-field to see
    what happens in the background by clicking <a href='#' id='toggleLink'>here</a>. 
    In addition you can <a href='demo.zip'>download this demo</a> or 
    <a href='multiselect.js'>view the MultipleSelect script</a>.

    <br /><br />
    
    <?php 
    
    if(isset($_POST['token'])) {
        
        echo "<h2>Post:</h2>";
        
        print_r($_POST['languages']);
        
        echo "<p><a href='index.php'>Reset</a></p>";
        
    } else {
    
    ?>
    
        <form action='index.php' method='post'>

            <!-- Brackets after select name, very important -->
            <select name="languages[]" id='myList' multiple='multiple' size='11'>
                <option value='ASP'>ASP</option>
                <option value='CSS' selected='selected'>CSS</option>
                <option value='CGI'>CGI</option>
                <option value='Flash'>Flash</option>
                <option value='HTML'>HTML</option>
                <option value='Java'>Java</option>
                <option value='JavaScript'>JavaScript</option>
                <option value='PHP'>PHP</option>
            </select>

            <input name="token" type="hidden" value="secret" />
            <input type="submit" value="Submit" />

        </form>
    
    <?php } ?>

</body>

</html>

