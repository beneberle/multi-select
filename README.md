MooTools MultiSelect
===========

A MooTools multi-select improvement adapted from [scanyours.com](http://weblog.scanyours.com/2010/06/06/a-customized-multiple-select-box-using-mootools/ "Title"). This plugin creates alternative HTML for multi-select  list form elements. Allows for customization and skinning.  

How to use
----------

To use MultiSelect, add lib/mootools.js, lib/mootoolsmore.j,s and multiselect.js to your document. Add a select element with an id and a name, making sure to add brackets after the name. Add a new MultipleSelect object instance and pass in the id of your select element.

	<select id="myList" name="languages[]" multiple="multiple"  size="8">
	   <option value="ASP">ASP</option>
	   <option value="CSS" selected="selected">CSS</option>
	   <option value="CGI">CGI</option>
	   <option value="Flash">Flash</option>
	   <option value="HTML">HTML</option>
	   <option value="Java">Java</option>
	   <option value="JavaScript">JavaScript</option>
	   <option value="PHP">PHP</option>
	</select>

Add instance by:

	document.addEvent('domready', function() {
		new MultipleSelect('myList');
	});
	
